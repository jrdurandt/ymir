# Ymir

Graphics rendering sandbox and experimental game engine in [Odin](https://odin-lang.org/)

> _"Out of Ymir's flesh was fashioned the earth,_
> _And the mountains were made of his bones;_
> _The sky from the frost cold giant's skull,_
> _And the ocean out of his blood."_ - [wikipedia](https://en.wikipedia.org/wiki/Ymir)

## Architecture

![architecture](./docs/images/architecture_v2.png)

To add:
- Particle Emitter
- Audio

## Getting Started
`git submodule update --init`

### Build mikktspace lib
`cd libs\mikktspace-odin\src`
`make`

### Build par_shapes lib
`cd libs\par_shapes-odin\src`
`make`


### Ensure the following odin libs are built:
- cgltf
- stb_image

### Install deps
- glfw (libglfw3-dev)
