package main

import "core:log"
import "core:mem"

import la "core:math/linalg"
import gl "vendor:OpenGL"

import ymir "ymir"

CAMERA_MOVE_SPEED :: 0.05
CAMERA_ROT_SPEED :: 0.5

main :: proc() {
	context.logger = log.create_console_logger(.Info)
	defer log.destroy_console_logger(context.logger)

	when ODIN_DEBUG {
		context.logger.lowest_level = .Debug

		track_alloc: mem.Tracking_Allocator
		mem.tracking_allocator_init(&track_alloc, context.allocator)
		defer mem.tracking_allocator_destroy(&track_alloc)

		context.allocator = mem.tracking_allocator(&track_alloc)
		defer {
			for _, leak in track_alloc.allocation_map {
				log.errorf("%v leaked %m\n", leak.location, leak.size)
			}

			for bad_free in track_alloc.bad_free_array {
				log.errorf(
					"%v allocation %p was freed badly\n",
					bad_free.location,
					bad_free.memory,
				)
			}
		}
	}
	log.debug("Debug enabled")

	config := ymir.config_load()
	ymir.window_init("Ymir", config.width, config.height)
	defer ymir.window_destroy()

	gl.ClearColor(0.15, 0.15, 0.25, 1.0)
	gl.Enable(gl.DEPTH_TEST)
	gl.Enable(gl.CULL_FACE)

	width, height := ymir.window_get_size()
	gl.Viewport(0, 0, i32(width), i32(height))

	camera := ymir.camera_create_perspective(
		{0, 0, 3},
		{0, 0, 0},
		{0, 1, 0},
		45,
		f32(config.width) / f32(config.height),
		0.1,
		100,
	)

	basic_fx := ymir.shader_program_create(
		"assets/shaders/basic.vert",
		"assets/shaders/basic.frag",
	)
	defer ymir.shader_program_delete(&basic_fx)

	primitive_fx := ymir.shader_program_create(
		"assets/shaders/primitive.vert",
		"assets/shaders/primitive.frag",
	)
	defer ymir.shader_program_delete(&primitive_fx)

	model := ymir.model_load("assets/models/DamagedHelmet.glb")
	defer ymir.model_delete(&model)

	sphere := ymir.primitive_create_sphere_mesh(8, 8, scale = {0.3, 0.3, 0.3})
	defer ymir.mesh_delete(&sphere)

	sky := ymir.sky_create()
	defer ymir.sky_delete(&sky)


	ambient_light := ymir.AmbientLight {
		color     = {1, 1, 1},
		intensity = 0.5,
	}

	directional_lights := []ymir.DirectionalLight {
		{color = {1, 1, 1}, intensity = 1, direction = {1, -1, 1}},
	}

	point_lights := []ymir.PointLight {
		{color = {1, 0, 0}, intensity = 1, position = {-10, 0, 0}},
		{color = {0, 0, 1}, intensity = 1, position = {10, 0, 0}},
	}

	loop: for !ymir.window_should_close() {
		time := ymir.get_time()

		ymir.window_poll_events()
		if e := ymir.window_query_event(ymir.WindowResizeEvent); e != nil {
			new_width := e.width
			new_height := e.height

			gl.Viewport(0, 0, new_width, new_height)
			camera.aspect_ratio = f32(new_width) / f32(new_height)
			ymir.camera_update_projection(&camera)
			log.debugf("Resized {}x{}", new_width, new_height)
		}

		if ymir.is_key_just_pressed(.ESCAPE) {
			break loop
		}

		{ 	// Free Camera
			if ymir.is_button_pressed(.RIGHT) {
				ymir.set_cursor_mode(.DISABLED)
			} else {
				ymir.set_cursor_mode(.NORMAL)
			}

			camera_move := [3]f32{0, 0, 0}
			camera_rot: [2]f32
			if ymir.get_cursor_mode() == .DISABLED {
				if ymir.is_key_pressed(.W) {
					camera_move.z += 1
				} else if ymir.is_key_pressed(.S) {
					camera_move.z -= 1
				} else if ymir.is_key_pressed(.A) {
					camera_move.x -= 1
				} else if ymir.is_key_pressed(.D) {
					camera_move.x += 1
				} else if ymir.is_key_pressed(.SPACE) {
					camera_move.y += 1
				} else if ymir.is_key_pressed(.LEFT_CONTROL) {
					camera_move.y -= 1
				}
				camera_move *= CAMERA_MOVE_SPEED

				camera_rot = ymir.get_mouse_position_delta() * CAMERA_ROT_SPEED

				ymir.camera_move(&camera, camera_move)
				ymir.camera_yaw(&camera, la.to_radians(-camera_rot.x))
				ymir.camera_pitch(&camera, la.to_radians(-camera_rot.y))
				ymir.camera_update_view(&camera)
			}
		}

		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		// Draw model
		ymir.use_shader_program(&basic_fx)
		ymir.set_uniform(&basic_fx, "matView", camera.view)
		ymir.set_uniform(&basic_fx, "matProjection", camera.projection)

		ymir.set_uniform(&basic_fx, "uViewPos", camera.position)
		ymir.apply_ambient_light(&basic_fx, ambient_light)
		ymir.apply_directional_lights(&basic_fx, directional_lights[:])
		ymir.apply_point_lights(&basic_fx, point_lights[:])

		ymir.set_uniform(
			&basic_fx,
			"matModel",
			la.MATRIX4F32_IDENTITY * la.matrix4_rotate(la.PI / 2, [3]f32{1, 0, 0}),
		)
		ymir.model_draw(&model)

		// Draw primitives
		ymir.use_shader_program(&primitive_fx)
		ymir.set_uniform(&primitive_fx, "matView", camera.view)
		ymir.set_uniform(&primitive_fx, "matProjection", camera.projection)


		point_lights[0].position.x = la.sin(time) * 5
		point_lights[0].position.z = la.cos(time) * 5

		point_lights[1].position.x = -la.sin(time) * 5
		point_lights[1].position.z = -la.cos(time) * 5

		for &point_light in point_lights {

			ymir.set_uniform(
				&primitive_fx,
				"matModel",
				la.MATRIX4F32_IDENTITY * la.matrix4_translate(point_light.position),
			)

			ymir.set_uniform(&primitive_fx, "uColor", point_light.color)
			ymir.mesh_draw(&sphere)
		}

		// Draw sky
		ymir.sky_draw(&sky, camera)

		ymir.window_swap_buffers()
	}
}

