#version 440 core

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matProjection;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexTexCoord;
layout(location = 2) in vec3 vertexNormal;
layout(location = 3) in vec3 vertexTangent;
layout(location = 4) in vec3 vertexBitangent;

out VS_OUT {
    vec3 fragPosition;
    vec2 fragTexCoord;
    mat3 fragTBN;
} vs_out;


void main(){
    vec4 position = matModel * vec4(vertexPosition, 1);
    gl_Position = matProjection * matView * position;

    vs_out.fragPosition = position.xyz;
    vs_out.fragTexCoord = vertexTexCoord;

    vec3 T = normalize(vec3(matModel * vec4(vertexTangent, 0.0)));
    vec3 B = normalize(vec3(matModel * vec4(vertexBitangent, 0.0)));
    vec3 N = normalize(vec3(matModel * vec4(vertexNormal, 0.0)));

    vs_out.fragTBN = mat3(T, B, N);
}
