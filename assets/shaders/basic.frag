#version 440 core

#define USE_BLINN_PHONG true

#define MAX_DIR_LIGHTS 4
#define MAX_POINT_LIGHTS 8

layout(binding = 0) uniform sampler2D uAlbedoTex;
layout(binding = 1) uniform sampler2D uNormalTex;

uniform vec3 uViewPos;

uniform struct AmbientLight {
    vec3 color;
    float intensity;
} uAmbientLight;

uniform struct DirectionalLight {
    vec3 direction;
    vec3 color;
    float intensity;
    bool enabled;
} uDirLights[MAX_DIR_LIGHTS];

uniform struct PointLight {
    vec3 position;
    vec3 color;
    float intensity;
    bool enabled;
} uPointLights[MAX_POINT_LIGHTS];

in VS_OUT {
    vec3 fragPosition;
    vec2 fragTexCoord;
    mat3 fragTBN;
} fs_in;

out vec4 outColor;

vec3 calc_directional_lights(vec3 normal, vec3 viewDir){
    vec3 lighting = vec3(0, 0, 0);
    for(int i = 0; i < MAX_DIR_LIGHTS; i++){
        DirectionalLight light = uDirLights[i];
        if(!light.enabled){
            continue;
        }

        vec3 lightDir = normalize(-light.direction);
        float diff = max(dot(normal, lightDir), 0.0);
        vec3 diffuse = diff * light.color * light.intensity;

        float spec = 0.0;
        if(USE_BLINN_PHONG) {
            // Blinn-Phong
            vec3 halfwayDir = normalize(lightDir + viewDir);
            spec = pow(max(dot(normal, halfwayDir), 0.0), 32);
        } else {
            // Blinn
            vec3 reflectDir = reflect(-lightDir, normal);
            spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
        }

        vec3 specular = light.intensity * spec * light.color;

        lighting += diffuse + specular;
    }
    return lighting;
}


vec3 calc_point_lights(vec3 normal, vec3 viewDir, vec3 position){
    vec3 lighting = vec3(0, 0, 0);
    for(int i = 0; i < MAX_POINT_LIGHTS; i++){
        PointLight light = uPointLights[i];
        if(!light.enabled){
            continue;
        }

        vec3 lightDir = normalize(light.position - position);
        float diff = max(dot(normal, lightDir), 0.0);
        vec3 diffuse = diff * light.color * light.intensity;

        float spec = 0.0;
        if(USE_BLINN_PHONG) {
            // Blinn-Phong
            vec3 halfwayDir = normalize(lightDir + viewDir);
            spec = pow(max(dot(normal, halfwayDir), 0.0), 32);
        } else {
            // Blinn
            vec3 reflectDir = reflect(-lightDir, normal);
            spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
        }
             
        vec3 specular = light.intensity * spec * light.color;

        lighting += diffuse + specular;
    }
    return lighting;
}

void main(){
    vec4 albedo = texture(uAlbedoTex, fs_in.fragTexCoord);

    vec3 normal = texture(uNormalTex, fs_in.fragTexCoord).rgb;
    normal = normal * 2.0 - 1.0;
    normal = normalize(fs_in.fragTBN * normal);

    vec3 position = fs_in.fragPosition;
    
    vec3 viewDir = normalize(uViewPos - position);
    vec3 lighting = uAmbientLight.color * uAmbientLight.intensity;
    lighting += calc_directional_lights(normal, viewDir);
    lighting += calc_point_lights(normal, viewDir, position);

    outColor = albedo * vec4(lighting, 1);
}
