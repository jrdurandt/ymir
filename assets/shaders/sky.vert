#version 440 core

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matProjection;

layout(location = 0) in vec3 vertexPosition;

out vec3 fragTexCoord;

void main(){
    fragTexCoord = vertexPosition;
    gl_Position = matProjection * matView * matModel * vec4(vertexPosition, 1);
}
