#version 440 core

uniform samplerCube uCubeMap;

in vec3 fragTexCoord;

out vec4 outColor;

void main(){
    // normal = normal * 2.0 - 1.0;
    vec3 texCoord = fragTexCoord * 2.0 - 1.0;
    outColor = texture(uCubeMap, texCoord);
}
