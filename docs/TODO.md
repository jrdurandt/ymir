# TODO

## Core:
- [x] Window
- [x] Input
- [ ] Audio

## GL
- [x] Shader Program
- [x] Mesh
- [x] Texture
- [ ] Framebuffer

## Misc:
- [x] Image
- [ ] Font

## Engine
- [ ] Renderer
- [ ] Scene
- [x] Camera
- [ ] Model
- [ ] Material
- [ ] Lighting
- [ ] GUI
- [ ] Particle Emitter

## Game
- [ ] Terrain
- [ ] Water
- [ ] Sky
- [ ] Weather
- [ ] Foilage
- [ ] Static Props
- [ ] Dynamic Props
- [ ] AI
- [ ] Scripting


## Enhancements
- [ ] Window modes (Windowed, Fullscreen etc)
- [ ] TextureCube
