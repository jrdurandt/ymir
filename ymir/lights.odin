package ymir

import "core:fmt"

Light :: struct {
	color:     [3]f32,
	intensity: f32,
}

AmbientLight :: struct {
	using base: Light,
}

DirectionalLight :: struct {
	using base: Light,
	direction:  [3]f32,
}

PointLight :: struct {
	using base: Light,
	position:   [3]f32,
}

apply_ambient_light :: proc(using fx: ^ShaderProgram, light: AmbientLight) {
	set_uniform(fx, "uAmbientLight.color", light.color)
	set_uniform(fx, "uAmbientLight.intensity", light.intensity)
}

apply_point_lights :: proc(using fx: ^ShaderProgram, lights: []PointLight) {
	for i in 0 ..< len(lights) {
		light := lights[i]

		light_enabled_uniform_name := fmt.aprintf("uPointLights[{}].enabled", i)
		defer delete(light_enabled_uniform_name)

		light_position_uniform_name := fmt.aprintf("uPointLights[{}].position", i)
		defer delete(light_position_uniform_name)

		light_color_uniform_name := fmt.aprintf("uPointLights[{}].color", i)
		defer delete(light_color_uniform_name)

		light_intensity_uniform_name := fmt.aprintf("uPointLights[{}].intensity", i)
		defer delete(light_intensity_uniform_name)

		set_uniform(fx, light_enabled_uniform_name, true)
		set_uniform(fx, light_position_uniform_name, light.position)
		set_uniform(fx, light_color_uniform_name, light.color)
		set_uniform(fx, light_intensity_uniform_name, light.intensity)
	}
}

apply_directional_lights :: proc(using fx: ^ShaderProgram, lights: []DirectionalLight) {
	for i in 0 ..< len(lights) {
		light := lights[i]

		light_enabled_uniform_name := fmt.aprintf("uDirLights[{}].enabled", i)
		defer delete(light_enabled_uniform_name)

		light_direction_uniform_name := fmt.aprintf("uDirLights[{}].direction", i)
		defer delete(light_direction_uniform_name)

		light_color_uniform_name := fmt.aprintf("uDirLights[{}].color", i)
		defer delete(light_color_uniform_name)

		light_intensity_uniform_name := fmt.aprintf("uDirLights[{}].intensity", i)
		defer delete(light_intensity_uniform_name)

		set_uniform(fx, light_enabled_uniform_name, true)
		set_uniform(fx, light_direction_uniform_name, light.direction)
		set_uniform(fx, light_color_uniform_name, light.color)
		set_uniform(fx, light_intensity_uniform_name, light.intensity)
	}
}

