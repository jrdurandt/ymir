package ymir

import "base:runtime"
import "core:log"
import "core:strings"

import gl "vendor:OpenGL"
import glfw "vendor:glfw"

GL_VERSION_MAJOR :: 4
GL_VERSION_MINOR :: 6

WindowResizeEvent :: struct {
	width, height: i32,
}

WindowKeyEvent :: struct {
	key:      Key,
	scancode: i32,
	action:   Action,
	mods:     Mods,
}

WindowMouseButtonEvent :: struct {
	button: Button,
	action: Action,
	mods:   Mods,
}

WindowMouseMoveEvent :: struct {
	x, y: f32,
}

WindowMouseScrollEvent :: struct {
	x, y: f32,
}

WindowEvent :: union {
	WindowResizeEvent,
	WindowKeyEvent,
	WindowMouseButtonEvent,
	WindowMouseMoveEvent,
	WindowMouseScrollEvent,
}

Window :: struct {
	handle: glfw.WindowHandle,
	events: [dynamic]WindowEvent,
}

window: ^Window

window_init :: proc(title: string, width, height: int) {
	glfw_init := glfw.Init()
	log.assert(glfw_init == true, "Failed to init GLFW")

	glfw.DefaultWindowHints()
	glfw.WindowHint(glfw.CONTEXT_VERSION_MAJOR, GL_VERSION_MAJOR)
	glfw.WindowHint(glfw.CONTEXT_VERSION_MINOR, GL_VERSION_MINOR)
	glfw.WindowHint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

	c_title := strings.clone_to_cstring(title)
	defer delete(c_title)

	window = new(Window)
	window.handle = glfw.CreateWindow(i32(width), i32(height), c_title, nil, nil)
	log.assert(window.handle != nil, "Failed to create GLFW window")

	glfw.MakeContextCurrent(window.handle)

	gl.load_up_to(GL_VERSION_MAJOR, GL_VERSION_MINOR, glfw.gl_set_proc_address)

	log.infof("OpenGL Renderer: {}", gl.GetString(gl.RENDERER))
	log.infof("OpenGl Version: {}", gl.GetString(gl.VERSION))

	window.events = make([dynamic]WindowEvent)
	glfw.SetWindowSizeCallback(
		window.handle,
		proc "c" (window_handle: glfw.WindowHandle, width, height: i32) {
			context = runtime.default_context()
			append(&window.events, WindowResizeEvent{width, height})
		},
	)

	glfw.SetKeyCallback(
		window.handle,
		proc "c" (window_handle: glfw.WindowHandle, key, scancode, action, mods: i32) {
			context = runtime.default_context()
			append(&window.events, WindowKeyEvent{Key(key), scancode, Action(action), Mods(mods)})
		},
	)

	glfw.SetMouseButtonCallback(
		window.handle,
		proc "c" (window_handle: glfw.WindowHandle, button, action, mods: i32) {
			context = runtime.default_context()
			append(
				&window.events,
				WindowMouseButtonEvent{Button(button), Action(action), Mods(mods)},
			)
		},
	)

	glfw.SetCursorPosCallback(
		window.handle,
		proc "c" (window_handle: glfw.WindowHandle, x_pos, y_pos: f64) {
			context = runtime.default_context()
			append(&window.events, WindowMouseMoveEvent{f32(x_pos), f32(y_pos)})
		},
	)

	glfw.SetScrollCallback(
		window.handle,
		proc "c" (window_handle: glfw.WindowHandle, x_offset, y_offset: f64) {
			context = runtime.default_context()
			append(&window.events, WindowMouseScrollEvent{f32(x_offset), f32(y_offset)})
		},
	)

	return
}

window_destroy :: proc() {
	delete(window.events)
	glfw.DestroyWindow(window.handle)
	glfw.Terminate()
	free(window)
}

window_should_close :: proc() -> bool {
	return glfw.WindowShouldClose(window.handle) == glfw.TRUE
}

window_swap_buffers :: proc() {
	glfw.SwapBuffers(window.handle)
}

window_poll_events :: proc() {
	clear(&window.events)
	glfw.PollEvents()
}

window_query_event :: proc($T: typeid) -> ^T {
	for e, i in window.events {
		#partial switch event in e {
		case T:
			return &window.events[i].(T)
		}
	}
	return nil
}

window_get_position :: proc() -> (x, y: int) {
	pos_x, pos_y := glfw.GetWindowPos(window.handle)
	x = int(pos_x)
	y = int(pos_y)
	return
}

window_get_framebuffer_size :: proc() -> (width, height: int) {
	w, h := glfw.GetFramebufferSize(window.handle)
	width = int(w)
	height = int(h)
	return
}

window_get_size :: proc() -> (width, height: int) {
	w, h := glfw.GetWindowSize(window.handle)
	width = int(w)
	height = int(h)
	return
}

window_get_frame_size :: proc() -> (left, top, right, bottom: int) {
	l, t, r, b := glfw.GetWindowFrameSize(window.handle)
	left = int(l)
	top = int(t)
	right = int(r)
	bottom = int(b)
	return
}

get_time :: proc() -> f32 {
	return f32(glfw.GetTime())
}

is_key_action :: proc(key: Key, action: Action) -> bool {
	return glfw.GetKey(window.handle, i32(key)) == i32(action)
}

is_key_pressed :: proc(key: Key) -> bool {
	return is_key_action(key, .PRESS)
}

is_key_released :: proc(key: Key) -> bool {
	return is_key_action(key, .RELEASE)
}

get_key_event :: proc(key: Key) -> (event: ^WindowKeyEvent, found: bool) {
	if e := window_query_event(WindowKeyEvent); e != nil {
		if e.key == key {
			event = e
			found = true
		}
	}
	return
}

is_key_just_action :: proc(key: Key, action: Action) -> bool {
	event, found := get_key_event(key)
	if !found {
		return false
	}
	return event.key == key && event.action == action
}

is_key_just_pressed :: proc(key: Key) -> bool {
	return is_key_just_action(key, .PRESS)
}

is_key_just_released :: proc(key: Key) -> bool {
	return is_key_just_action(key, .RELEASE)
}

is_button_action :: proc(button: Button, action: Action) -> bool {
	return glfw.GetMouseButton(window.handle, i32(button)) == i32(action)
}

is_button_pressed :: proc(button: Button) -> bool {
	return is_button_action(button, .PRESS)
}

is_button_released :: proc(button: Button) -> bool {
	return is_button_action(button, .RELEASE)
}

get_button_event :: proc(button: Button) -> (event: ^WindowMouseButtonEvent, found: bool) {
	if e := window_query_event(WindowMouseButtonEvent); e != nil {
		if e.button == button {
			event = e
			found = true
		}
	}
	return
}

is_button_just_action :: proc(button: Button, action: Action) -> bool {
	event, found := get_button_event(button)
	if !found {
		return false
	}
	return event.button == button && event.action == action
}

is_button_just_pressed :: proc(button: Button) -> bool {
	return is_button_just_action(button, .PRESS)
}

is_button_just_released :: proc(button: Button) -> bool {
	return is_button_just_action(button, .RELEASE)
}

get_mouse_position :: proc() -> [2]f32 {
	pos_x, pos_y := glfw.GetCursorPos(window.handle)
	return {f32(pos_x), f32(pos_y)}
}

get_cursor_mode :: proc() -> CursorMode {
	return CursorMode(glfw.GetInputMode(window.handle, glfw.CURSOR))
}

set_cursor_mode :: proc(mode: CursorMode) {
	glfw.SetInputMode(window.handle, glfw.CURSOR, i32(mode))
}

enable_raw_mouse_motion :: proc() {
	if glfw.RawMouseMotionSupported() {
		glfw.SetInputMode(window.handle, glfw.RAW_MOUSE_MOTION, 1)
	}
}

get_mouse_position_delta :: proc() -> [2]f32 {
	prev_pos := get_mouse_position()
	if e := window_query_event(WindowMouseMoveEvent); e != nil {
		prev_pos = {e.x, e.y}
	}
	curr_pos := get_mouse_position()
	return curr_pos - prev_pos
}

get_mouse_scroll_delta :: proc() -> (delta: [2]f32) {
	if e := window_query_event(WindowMouseScrollEvent); e != nil {
		delta = {e.x, e.y}
	}
	return
}

Action :: enum (i32) {
	RELEASE = glfw.RELEASE,
	PRESS   = glfw.PRESS,
	REPEAT  = glfw.REPEAT,
}

Mods :: enum (i32) {
	SHIFT     = glfw.MOD_SHIFT,
	CONTROL   = glfw.MOD_CONTROL,
	ALT       = glfw.MOD_ALT,
	SUPER     = glfw.MOD_SUPER,
	CAPS_LOCK = glfw.MOD_CAPS_LOCK,
	NUM_LOCK  = glfw.MOD_NUM_LOCK,
}

Key :: enum (i32) {
	//Named printable keys
	SPACE         = glfw.KEY_SPACE,
	APOSTROPHE    = glfw.KEY_APOSTROPHE,
	COMMA         = glfw.KEY_COMMA,
	MINUS         = glfw.KEY_MINUS,
	PERIOD        = glfw.KEY_PERIOD,
	SLASH         = glfw.KEY_SLASH,
	SEMICOLON     = glfw.KEY_SEMICOLON,
	EQUAL         = glfw.KEY_EQUAL,
	LEFT_BRACKET  = glfw.KEY_LEFT_BRACKET,
	BACKSLASH     = glfw.KEY_BACKSLASH,
	RIGHT_BRACKET = glfw.KEY_RIGHT_BRACKET,
	GRAVE_ACCENT  = glfw.KEY_GRAVE_ACCENT,
	WORLD_1       = glfw.KEY_WORLD_1,
	WORLD_2       = glfw.KEY_WORLD_2,

	//Named non-printable keys
	ESCAPE        = glfw.KEY_ESCAPE,
	ENTER         = glfw.KEY_ENTER,
	TAB           = glfw.KEY_TAB,
	BACKSPACE     = glfw.KEY_BACKSPACE,
	INSERT        = glfw.KEY_INSERT,
	DELETE        = glfw.KEY_DELETE,
	RIGHT         = glfw.KEY_RIGHT,
	LEFT          = glfw.KEY_LEFT,
	DOWN          = glfw.KEY_DOWN,
	UP            = glfw.KEY_UP,
	PAGE_UP       = glfw.KEY_PAGE_UP,
	PAGE_DOWN     = glfw.KEY_PAGE_DOWN,
	HOME          = glfw.KEY_HOME,
	END           = glfw.KEY_END,
	CAPS_LOCK     = glfw.KEY_CAPS_LOCK,
	SCROLL_LOCK   = glfw.KEY_SCROLL_LOCK,
	NUM_LOCK      = glfw.KEY_NUM_LOCK,
	PRINT_SCREEN  = glfw.KEY_PRINT_SCREEN,
	PAUSE         = glfw.KEY_PAUSE,

	//Alphanumeric characters
	KEY_0         = glfw.KEY_0,
	KEY_1         = glfw.KEY_1,
	KEY_2         = glfw.KEY_2,
	KEY_3         = glfw.KEY_3,
	KEY_4         = glfw.KEY_4,
	KEY_5         = glfw.KEY_5,
	KEY_6         = glfw.KEY_6,
	KEY_7         = glfw.KEY_7,
	KEY_8         = glfw.KEY_8,
	KEY_9         = glfw.KEY_9,

	//Alphabet
	A             = glfw.KEY_A,
	B             = glfw.KEY_B,
	C             = glfw.KEY_C,
	D             = glfw.KEY_D,
	E             = glfw.KEY_E,
	F             = glfw.KEY_F,
	G             = glfw.KEY_G,
	H             = glfw.KEY_H,
	I             = glfw.KEY_I,
	J             = glfw.KEY_J,
	K             = glfw.KEY_K,
	L             = glfw.KEY_L,
	M             = glfw.KEY_M,
	N             = glfw.KEY_N,
	O             = glfw.KEY_O,
	P             = glfw.KEY_P,
	Q             = glfw.KEY_Q,
	R             = glfw.KEY_R,
	S             = glfw.KEY_S,
	T             = glfw.KEY_T,
	U             = glfw.KEY_U,
	V             = glfw.KEY_V,
	W             = glfw.KEY_W,
	X             = glfw.KEY_X,
	Y             = glfw.KEY_Y,
	Z             = glfw.KEY_Z,

	//Functions
	F1            = glfw.KEY_F1,
	F2            = glfw.KEY_F2,
	F3            = glfw.KEY_F3,
	F4            = glfw.KEY_F4,
	F5            = glfw.KEY_F5,
	F6            = glfw.KEY_F6,
	F7            = glfw.KEY_F7,
	F8            = glfw.KEY_F8,
	F9            = glfw.KEY_F9,
	F10           = glfw.KEY_F10,
	F11           = glfw.KEY_F11,
	F12           = glfw.KEY_F12,
	F13           = glfw.KEY_F13,
	F14           = glfw.KEY_F14,
	F15           = glfw.KEY_F15,
	F16           = glfw.KEY_F16,
	F17           = glfw.KEY_F17,
	F18           = glfw.KEY_F18,
	F19           = glfw.KEY_F19,
	F20           = glfw.KEY_F20,
	F21           = glfw.KEY_F21,
	F22           = glfw.KEY_F22,
	F23           = glfw.KEY_F23,
	F24           = glfw.KEY_F24,
	F25           = glfw.KEY_F25,

	//Keypad number
	KP_0          = glfw.KEY_KP_0,
	KP_1          = glfw.KEY_KP_1,
	KP_2          = glfw.KEY_KP_2,
	KP_3          = glfw.KEY_KP_3,
	KP_4          = glfw.KEY_KP_4,
	KP_5          = glfw.KEY_KP_5,
	KP_6          = glfw.KEY_KP_6,
	KP_7          = glfw.KEY_KP_7,
	KP_8          = glfw.KEY_KP_8,
	KP_9          = glfw.KEY_KP_9,

	//Keypad named function keys
	KP_DECIMAL    = glfw.KEY_KP_DECIMAL,
	KP_DIVIDE     = glfw.KEY_KP_DIVIDE,
	KP_MULTIPLY   = glfw.KEY_KP_MULTIPLY,
	KP_SUBTRACT   = glfw.KEY_KP_SUBTRACT,
	KP_ADD        = glfw.KEY_KP_ADD,
	KP_ENTER      = glfw.KEY_KP_ENTER,
	KP_EQUAL      = glfw.KEY_KP_EQUAL,

	//Modifier keys
	LEFT_SHIFT    = glfw.KEY_LEFT_SHIFT,
	LEFT_CONTROL  = glfw.KEY_LEFT_CONTROL,
	LEFT_ALT      = glfw.KEY_LEFT_ALT,
	LEFT_SUPER    = glfw.KEY_LEFT_SUPER,
	RIGHT_SHIFT   = glfw.KEY_RIGHT_SHIFT,
	RIGHT_CONTROL = glfw.KEY_RIGHT_CONTROL,
	RIGHT_ALT     = glfw.KEY_RIGHT_ALT,
	RIGHT_SUPER   = glfw.KEY_RIGHT_SUPER,
	MENU          = glfw.KEY_MENU,
}

Button :: enum (i32) {
	//Buttons
	BUTTON_1 = glfw.MOUSE_BUTTON_1,
	BUTTON_2 = glfw.MOUSE_BUTTON_2,
	BUTTON_3 = glfw.MOUSE_BUTTON_3,
	BUTTON_4 = glfw.MOUSE_BUTTON_4,
	BUTTON_5 = glfw.MOUSE_BUTTON_5,
	BUTTON_6 = glfw.MOUSE_BUTTON_6,
	BUTTON_7 = glfw.MOUSE_BUTTON_7,
	BUTTON_8 = glfw.MOUSE_BUTTON_8,

	//Aliases
	LAST     = glfw.MOUSE_BUTTON_8,
	LEFT     = glfw.MOUSE_BUTTON_1,
	RIGHT    = glfw.MOUSE_BUTTON_2,
	MIDDLE   = glfw.MOUSE_BUTTON_3,
}

CursorMode :: enum (i32) {
	NORMAL   = glfw.CURSOR_NORMAL,
	HIDDEN   = glfw.CURSOR_HIDDEN,
	DISABLED = glfw.CURSOR_DISABLED,
	CAPTURED = glfw.CURSOR_CAPTURED,
}

