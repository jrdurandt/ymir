package ymir

import "core:log"

import gl "vendor:OpenGL"

Texture :: struct {
	id:     u32,
	target: u32,
}

Texture2D :: struct {
	width, height: i32,
	using base:    Texture,
}

TextureCubeMap :: struct {
	using base: Texture,
}

TextureParams :: struct {
	wrap_s:      TextureWrap,
	wrap_t:      TextureWrap,
	wrap_r:      TextureWrap,
	min_filter:  TextureFilter,
	mag_filter:  TextureFilter,
	gen_mipmaps: bool,
}

DEFAULT_TEXTURE_PARAMS := TextureParams {
	wrap_s      = .REPEAT,
	wrap_t      = .REPEAT,
	wrap_r      = .REPEAT,
	min_filter  = .LINEAR,
	mag_filter  = .LINEAR,
	gen_mipmaps = false,
}


texture_create_2d_raw :: proc(
	texture_format: TextureInternalFormat,
	width, height: i32,
	pixel_format: TextureFormat,
	pixel_type: Type,
	pixel_data: rawptr,
	params := DEFAULT_TEXTURE_PARAMS,
) -> (
	texture: Texture2D,
) {
	texture.target = gl.TEXTURE_2D
	texture.width = width
	texture.height = height

	gl.CreateTextures(texture.target, 1, &texture.id)
	gl.BindTexture(texture.target, texture.id)
	gl.TexImage2D(
		texture.target,
		0,
		i32(texture_format),
		width,
		height,
		0,
		u32(pixel_format),
		u32(pixel_type),
		pixel_data,
	)

	gl.TextureParameteri(texture.id, gl.TEXTURE_WRAP_S, i32(params.wrap_s))
	gl.TextureParameteri(texture.id, gl.TEXTURE_WRAP_T, i32(params.wrap_t))
	gl.TextureParameteri(texture.id, gl.TEXTURE_MIN_FILTER, i32(params.min_filter))
	gl.TextureParameteri(texture.id, gl.TEXTURE_MAG_FILTER, i32(params.mag_filter))

	if params.gen_mipmaps {
		gl.GenerateMipmap(texture.target)
	}

	gl.BindTexture(texture.target, 0)
	return
}

texture_create_2d_from_image :: proc(
	using image: ^Image,
	params := DEFAULT_TEXTURE_PARAMS,
) -> Texture2D {
	return texture_create_2d_raw(
		internal_format,
		width,
		height,
		format,
		.UNSIGNED_BYTE,
		raw_data(data),
		params,
	)
}

texture_create_2d_from_path :: proc(path: string, params := DEFAULT_TEXTURE_PARAMS) -> Texture2D {
	img := image_load(path)
	return texture_create_2d_from_image(&img)
}

texture_create_2d :: proc {
	texture_create_2d_raw,
	texture_create_2d_from_image,
	texture_create_2d_from_path,
}

texture_create_cube_map_raw :: proc(
	texture_format: TextureInternalFormat,
	width, height: i32,
	pixel_format: TextureFormat,
	pixel_type: Type,
	pos_x_data: rawptr,
	neg_x_data: rawptr,
	pos_y_data: rawptr,
	neg_y_data: rawptr,
	pos_z_data: rawptr,
	neg_z_data: rawptr,
	params := DEFAULT_TEXTURE_PARAMS,
) -> (
	texture: TextureCubeMap,
) {

	texture.target = gl.TEXTURE_CUBE_MAP

	gl.CreateTextures(texture.target, 1, &texture.id)
	gl.BindTexture(texture.target, texture.id)

	pixel_data := [?]rawptr{pos_x_data, neg_x_data, pos_y_data, neg_y_data, pos_z_data, neg_z_data}
	for data, i in pixel_data {
		gl.TexImage2D(
			gl.TEXTURE_CUBE_MAP_POSITIVE_X + u32(i),
			0,
			i32(texture_format),
			width,
			height,
			0,
			u32(pixel_format),
			u32(pixel_type),
			data,
		)
	}

	gl.TextureParameteri(texture.id, gl.TEXTURE_WRAP_S, i32(params.wrap_s))
	gl.TextureParameteri(texture.id, gl.TEXTURE_WRAP_T, i32(params.wrap_t))
	gl.TextureParameteri(texture.id, gl.TEXTURE_WRAP_R, i32(params.wrap_r))
	gl.TextureParameteri(texture.id, gl.TEXTURE_MIN_FILTER, i32(params.min_filter))
	gl.TextureParameteri(texture.id, gl.TEXTURE_MAG_FILTER, i32(params.mag_filter))

	if params.gen_mipmaps {
		gl.GenerateMipmap(texture.target)
	}

	gl.BindTexture(texture.target, 0)
	return
}

texture_create_cube_map_from_image :: proc(
	pos_x_image: ^Image,
	neg_x_image: ^Image,
	pos_y_image: ^Image,
	neg_y_image: ^Image,
	pos_z_image: ^Image,
	neg_z_image: ^Image,
	params := DEFAULT_TEXTURE_PARAMS,
) -> TextureCubeMap {
	return texture_create_cube_map_raw(
		pos_x_image.internal_format,
		pos_x_image.width,
		pos_x_image.height,
		pos_x_image.format,
		.UNSIGNED_BYTE,
		raw_data(pos_x_image.data),
		raw_data(neg_x_image.data),
		raw_data(pos_y_image.data),
		raw_data(neg_y_image.data),
		raw_data(pos_z_image.data),
		raw_data(neg_z_image.data),
		params,
	)
}

texture_create_cube_map_from_path :: proc(
	pos_x_path: string,
	neg_x_path: string,
	pos_y_path: string,
	neg_y_path: string,
	pos_z_path: string,
	neg_z_path: string,
	params := DEFAULT_TEXTURE_PARAMS,
) -> TextureCubeMap {
	pos_x_image := image_load(pos_x_path)
	neg_x_image := image_load(neg_x_path)
	pos_y_image := image_load(pos_y_path)
	neg_y_image := image_load(neg_y_path)
	pos_z_image := image_load(pos_z_path)
	neg_z_image := image_load(neg_z_path)
	return texture_create_cube_map_from_image(
		&pos_x_image,
		&neg_x_image,
		&pos_y_image,
		&neg_y_image,
		&pos_z_image,
		&neg_z_image,
		params,
	)
}

texture_create_cube_map :: proc {
	texture_create_cube_map_raw,
	texture_create_cube_map_from_image,
	texture_create_cube_map_from_path,
}

texture_delete :: proc(using self: ^Texture) {
	gl.DeleteTextures(1, &id)
}

bind_texture :: proc(using self: ^Texture, unit: u32) {
	gl.BindTextureUnit(unit, id)
}

