package ymir

import gl "vendor:OpenGL"

Topology :: enum (u32) {
	POINTS         = gl.POINTS,
	LINES          = gl.LINES,
	LINE_LOOP      = gl.LINE_LOOP,
	LINE_STRIP     = gl.LINE_STRIP,
	TRIANGLES      = gl.TRIANGLES,
	TRIANGLE_STRIP = gl.TRIANGLE_STRIP,
	TRIANGLE_FAN   = gl.TRIANGLE_FAN,
}

Type :: enum (u32) {
	BYTE           = gl.BYTE,
	UNSIGNED_BYTE  = gl.UNSIGNED_BYTE,
	SHORT          = gl.SHORT,
	UNSIGNED_SHORT = gl.UNSIGNED_SHORT,
	INT            = gl.INT,
	UNSIGNED_INT   = gl.UNSIGNED_INT,
	HALF_FLOAT     = gl.HALF_FLOAT,
	FLOAT          = gl.FLOAT,
	DOUBLE         = gl.DOUBLE,
}

TextureFilter :: enum (u32) {
	NEAREST                = gl.NEAREST,
	LINEAR                 = gl.LINEAR,
	NEAREST_MIPMAP_NEAREST = gl.NEAREST_MIPMAP_NEAREST,
	LINEAR_MIPMAP_NEAREST  = gl.LINEAR_MIPMAP_NEAREST,
	NEAREST_MIPMAP_LINEAR  = gl.NEAREST_MIPMAP_LINEAR,
	LINEAR_MIPMAP_LINEAR   = gl.LINEAR_MIPMAP_LINEAR,
}

TextureWrap :: enum (u32) {
	CLAMP_TO_EDGE        = gl.CLAMP_TO_EDGE,
	CLAMP_TO_BORDER      = gl.CLAMP_TO_BORDER,
	REPEAT               = gl.REPEAT,
	MIRRORED_REPEAT      = gl.MIRRORED_REPEAT,
	MIRROR_CLAMP_TO_EDGE = gl.MIRROR_CLAMP_TO_EDGE,
}

TextureInternalFormat :: enum (u32) {
	// Base internal formats
	DEPTH_COMPONENT    = gl.DEPTH_COMPONENT,
	DEPTH_STENCIL      = gl.DEPTH_STENCIL,
	RED                = gl.RED,
	RG                 = gl.RG,
	RGB                = gl.RGB,
	RGBA               = gl.RGBA,

	// Sized internal formats
	R8                 = gl.R8,
	R8_SNORM           = gl.R8_SNORM,
	R16                = gl.R16,
	R16_SNORM          = gl.R16_SNORM,
	RG8                = gl.RG8,
	RG8_SNORM          = gl.RG8_SNORM,
	RG16               = gl.RG16,
	RG16_SNORM         = gl.RG16_SNORM,
	R3_G3_B2           = gl.R3_G3_B2,
	RGB4               = gl.RGB4,
	RGB5               = gl.RGB5,
	RGB8               = gl.RGB8,
	RGB8_SNORM         = gl.RGB8_SNORM,
	RGB10              = gl.RGB10,
	RGB12              = gl.RGB12,
	RGB16_SNORM        = gl.RGB16_SNORM,
	RGBA2              = gl.RGBA2,
	RGBA4              = gl.RGBA4,
	RGB5_A1            = gl.RGB5_A1,
	RGBA8              = gl.RGBA8,
	RGBA8_SNORM        = gl.RGBA8_SNORM,
	RGB10_A2           = gl.RGB10_A2,
	RGB10_A2UI         = gl.RGB10_A2UI,
	RGBA12             = gl.RGBA12,
	RGBA16             = gl.RGBA16,
	SRGB8              = gl.SRGB8,
	SRGB8_ALPHA8       = gl.SRGB8_ALPHA8,

	// Floating point formats
	R16F               = gl.R16F,
	RG16F              = gl.RG16F,
	RGB16F             = gl.RGB16F,
	RGBA16F            = gl.RGBA16F,
	R32F               = gl.R32F,
	RG32F              = gl.RG32F,
	RGB32F             = gl.RGB32F,
	RGBA32F            = gl.RGBA32F,
	R11F_G11F_B10F     = gl.R11F_G11F_B10F,
	RGB9_E5            = gl.RGB9_E5,

	// Unsigned integer formats
	R8I                = gl.R8I,
	R8UI               = gl.R8UI,
	R16I               = gl.R16I,
	R16UI              = gl.R16UI,
	R32I               = gl.R32I,
	R32UI              = gl.R32UI,
	RG8I               = gl.RG8I,
	RG8UI              = gl.RG8UI,
	RG16I              = gl.RG16I,
	RG16UI             = gl.RG16UI,
	RG32I              = gl.RG32I,
	RG32UI             = gl.RG32UI,
	RGB8I              = gl.RGB8I,
	RGB8UI             = gl.RGB8UI,
	RGB16I             = gl.RGB16I,
	RGB16UI            = gl.RGB16UI,
	RGB32I             = gl.RGB32I,
	RGB32UI            = gl.RGB32UI,
	RGBA8I             = gl.RGBA8I,
	RGBA8UI            = gl.RGBA8UI,
	RGBA16I            = gl.RGBA16I,
	RGBA16UI           = gl.RGBA16UI,
	RGBA32I            = gl.RGBA32I,
	RGBA32UI           = gl.RGBA32UI,

	// Depth and stencil formats
	DEPTH_COMPONENT16  = gl.DEPTH_COMPONENT16,
	DEPTH_COMPONENT24  = gl.DEPTH_COMPONENT24,
	DEPTH_COMPONENT32  = gl.DEPTH_COMPONENT32,
	DEPTH_COMPONENT32F = gl.DEPTH_COMPONENT32F,
	DEPTH24_STENCIL8   = gl.DEPTH24_STENCIL8,
	DEPTH32F_STENCIL8  = gl.DEPTH32F_STENCIL8,
	STENCIL_INDEX8     = gl.STENCIL_INDEX8,
}

TextureFormat :: enum (u32) {
	// Base formats
	RED                                = gl.RED,
	RG                                 = gl.RG,
	RGB                                = gl.RGB,
	BGR                                = gl.BGR,
	RGBA                               = gl.RGBA,
	BGRA                               = gl.BGRA,

	// Depth and stencil formats
	DEPTH_COMPONENT                    = gl.DEPTH_COMPONENT,
	DEPTH_STENCIL                      = gl.DEPTH_STENCIL,

	// Compressed formats
	COMPRESSED_RED                     = gl.COMPRESSED_RED,
	COMPRESSED_RG                      = gl.COMPRESSED_RG,
	COMPRESSED_RGB                     = gl.COMPRESSED_RGB,
	COMPRESSED_RGBA                    = gl.COMPRESSED_RGBA,
	COMPRESSED_SRGB                    = gl.COMPRESSED_SRGB,
	COMPRESSED_SRGB_ALPHA              = gl.COMPRESSED_SRGB_ALPHA,
	COMPRESSED_RED_RGTC1               = gl.COMPRESSED_RED_RGTC1,
	COMPRESSED_SIGNED_RED_RGTC1        = gl.COMPRESSED_SIGNED_RED_RGTC1,
	COMPRESSED_RG_RGTC2                = gl.COMPRESSED_RG_RGTC2,
	COMPRESSED_SIGNED_RG_RGTC2         = gl.COMPRESSED_SIGNED_RG_RGTC2,
	COMPRESSED_RGBA_BPTC_UNORM         = gl.COMPRESSED_RGBA_BPTC_UNORM,
	COMPRESSED_SRGB_ALPHA_BPTC_UNORM   = gl.COMPRESSED_SRGB_ALPHA_BPTC_UNORM,
	COMPRESSED_RGB_BPTC_SIGNED_FLOAT   = gl.COMPRESSED_RGB_BPTC_SIGNED_FLOAT,
	COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT = gl.COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT,

	// Integer formats
	RED_INTEGER                        = gl.RED_INTEGER,
	RG_INTEGER                         = gl.RG_INTEGER,
	RGB_INTEGER                        = gl.RGB_INTEGER,
	BGR_INTEGER                        = gl.BGR_INTEGER,
	RGBA_INTEGER                       = gl.RGBA_INTEGER,
	BGRA_INTEGER                       = gl.BGRA_INTEGER,
}

