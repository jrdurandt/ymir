package ymir

import json "core:encoding/json"

import "core:log"
import "core:os"
import "core:strconv"

CONFIG_FILENAME :: "config.json"

Config :: struct {
	width:  int,
	height: int,
}

config_load :: proc(filename: string = CONFIG_FILENAME) -> (config: Config) {
	data, ok := os.read_entire_file_from_filename(filename)
	log.assertf(ok, "Failed to load config file: {}", filename)
	defer delete(data)

	json_data, err := json.parse(data)
	log.assertf(err == .None, "Failed to parse config json file [{}]: {}", filename, err)
	defer json.destroy_value(json_data)

	root := json_data.(json.Object)

	window := root["window"].(json.Object)

	config.width = int(window["width"].(json.Float))
	config.height = int(window["height"].(json.Float))

	log.debugf("Loaded config [{}]: {}", filename, config)
	return
}

