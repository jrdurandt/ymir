package ymir

import "core:log"
import "core:os"

import gl "vendor:OpenGL"
import gltf "vendor:cgltf"
import stbi "vendor:stb/image"

ImageFormat :: enum {
	GRAYSCALE = gl.RED,
	RGB       = gl.RGB,
	RGBA      = gl.RGBA,
}

Image :: struct {
	width, height:   i32,
	channels:        i32,
	internal_format: TextureInternalFormat,
	format:          TextureFormat,
	data:            []byte,
}

image_load_from_memory :: proc(data: []byte) -> (image: Image) {
	image_data := stbi.load_from_memory(
		raw_data(data),
		i32(len(data)),
		&image.width,
		&image.height,
		&image.channels,
		0,
	)

	log.assertf(image_data != nil, "Failed to load image: {}", stbi.failure_reason())

	image.data = image_data[0:(image.width * image.height * image.channels)]
	switch image.channels {
	case 1:
		image.internal_format = .RED
		image.format = .RED
	case 3:
		image.internal_format = .RGB
		image.format = .RGB
	case 4:
		image.internal_format = .RGBA
		image.format = .RGBA
	}
	return
}

image_load_from_path :: proc(path: string) -> Image {
	data, success := os.read_entire_file(path)
	log.assertf(success, "Failed to load image file: {}", path)
	defer delete(data)

	return image_load_from_memory(data)
}

image_load_gltf :: proc(gltf_image: ^gltf.image) -> (image: Image) {
	//TODO: Currently only supports embedded images. Need to also support loading of material images from uri
	size := gltf_image.buffer_view.size
	offset := gltf_image.buffer_view.offset
	stride := gltf_image.buffer_view.stride != 0 ? gltf_image.buffer_view.stride : 1

	data := make([]u8, size)
	defer delete(data)
	for i in 0 ..< gltf_image.buffer_view.size {
		data[i] = ([^]u8)(gltf_image.buffer_view.buffer.data)[offset]
		offset += stride
	}
	image = image_load(data)

	return

}

image_load :: proc {
	image_load_from_memory,
	image_load_from_path,
	image_load_gltf,
}

image_free :: proc(using image: ^Image) {
	free(raw_data(data))
}

