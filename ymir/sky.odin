package ymir

import la "core:math/linalg"

import gl "vendor:OpenGL"

SKY_BOX_SIZE :: 100

Sky :: struct {
	fx:      ShaderProgram,
	mesh:    Mesh(PrimitiveVertex),
	texture: TextureCubeMap,
}

sky_create :: proc() -> (sky: Sky) {
	sky.fx = shader_program_create("assets/shaders/sky.vert", "assets/shaders/sky.frag")
	sky.mesh = primitive_create_cube_mesh(
		translation = {-0.5, -0.5, -0.5},
		rotation = {0, 0, 0, 0},
		scale = {SKY_BOX_SIZE, SKY_BOX_SIZE, SKY_BOX_SIZE},
	)

	sky.texture = texture_create_cube_map(
		"assets/textures/sky_right.jpg",
		"assets/textures/sky_left.jpg",
		"assets/textures/sky_top.jpg",
		"assets/textures/sky_bottom.jpg",
		"assets/textures/sky_front.jpg",
		"assets/textures/sky_back.jpg",
		TextureParams {
			wrap_s = .CLAMP_TO_EDGE,
			wrap_t = .CLAMP_TO_EDGE,
			wrap_r = .CLAMP_TO_EDGE,
			min_filter = .LINEAR,
			mag_filter = .LINEAR,
		},
	)

	return
}

sky_delete :: proc(using sky: ^Sky) {
	shader_program_delete(&fx)
	mesh_delete(&mesh)
	texture_delete(&texture)
}

sky_draw :: proc(using sky: ^Sky, camera: Camera) {
	gl.FrontFace(gl.CW)
	gl.DepthMask(gl.FALSE)

	use_shader_program(&fx)
	set_uniform(&fx, "matView", camera.view)
	set_uniform(&fx, "matProjection", camera.projection)
	set_uniform(&fx, "matModel", la.MATRIX4F32_IDENTITY * la.matrix4_translate(camera.position))

	set_uniform(&fx, "uCubeMap", 0)
	bind_texture(&texture, 0)

	mesh_draw(&mesh)

	gl.DepthMask(gl.TRUE)
	gl.FrontFace(gl.CCW)
}

