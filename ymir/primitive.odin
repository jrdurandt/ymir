package ymir

import "core:log"
import "core:slice"

import par "../libs/par_shapes-odin"

PrimitiveVertex :: struct {
	position: [3]f32,
}

PRIMITIVE_VERTEX_ATTRIBS :: []VertexAttribDesc {
	{
		index = 0,
		size = 3,
		type = .FLOAT,
		normalize = false,
		offset = offset_of(PrimitiveVertex, position),
	},
}

primitive_create_plane_mesh :: proc(
	slices, stacks: i32,
	translation := [3]f32{0, 0, 0},
	rotation := [4]f32{0, 0, 0, 0},
	scale := [3]f32{1, 1, 1},
) -> Mesh(PrimitiveVertex) {
	shape := par.create_plane(slices, stacks)
	primitive_shape_transform(shape, translation, rotation, scale)
	return primitive_generate_mesh(shape)
}

primitive_create_sphere_mesh :: proc(
	slices, stacks: i32,
	translation := [3]f32{0, 0, 0},
	rotation := [4]f32{0, 0, 0, 0},
	scale := [3]f32{1, 1, 1},
) -> Mesh(PrimitiveVertex) {
	shape := par.create_parametric_sphere(slices, stacks)
	primitive_shape_transform(shape, translation, rotation, scale)
	return primitive_generate_mesh(shape)
}

primitive_create_cube_mesh :: proc(
	translation := [3]f32{0, 0, 0},
	rotation := [4]f32{0, 0, 0, 0},
	scale := [3]f32{1, 1, 1},
) -> Mesh(PrimitiveVertex) {
	shape := par.create_cube()
	primitive_shape_transform(shape, translation, rotation, scale)
	return primitive_generate_mesh(shape)
}

primitive_shape_transform :: proc(
	shape: ^par.ParShapeMesh,
	translation: [3]f32,
	rotation: [4]f32,
	scale: [3]f32,
) {
	par.translate(shape, translation.x, translation.y, translation.z)
	par.rotate_ex(shape, rotation.w, rotation.x, rotation.y, rotation.z)
	par.scale(shape, scale.x, scale.y, scale.z)
}

@(private)
primitive_generate_mesh :: proc(shape: ^par.ParShapeMesh) -> Mesh(PrimitiveVertex) {
	vertex_count := shape.npoints
	par_positions := shape.points[0:vertex_count * 3]

	indices := shape.triangles[0:shape.ntriangles * 3]

	vertices := make([]PrimitiveVertex, vertex_count)
	defer delete(vertices)

	p := 0
	for v in 0 ..< vertex_count {
		vertices[v] = PrimitiveVertex {
			position = [3]f32{par_positions[p], par_positions[p + 1], par_positions[p + 2]},
		}
		p += 3
	}

	return mesh_create(vertices, indices, PRIMITIVE_VERTEX_ATTRIBS)
}

