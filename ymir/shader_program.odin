package ymir

import "core:log"
import "core:strings"

import gl "vendor:OpenGL"

ShaderProgram :: struct {
	id:       u32,
	uniforms: gl.Uniforms,
}

shader_program_create :: proc(vs_path, fs_path: string) -> ShaderProgram {
	program_id, ok := gl.load_shaders_file(vs_path, fs_path)
	compile_message, compile_type, link_message, link_type := gl.get_last_error_messages()
	log.assertf(
		ok,
		"Failed to create shader program [{}]: {}",
		link_type,
		link_type == .SHADER_LINK ? link_message : compile_message,
	)

	return ShaderProgram{id = program_id, uniforms = gl.get_uniforms_from_program(program_id)}
}

shader_program_delete :: proc(using self: ^ShaderProgram) {
	gl.destroy_uniforms(uniforms)
	gl.DeleteProgram(id)
}

use_shader_program :: proc(using self: ^ShaderProgram) {
	gl.UseProgram(id)
}

get_uniform_location :: proc(
	using self: ^ShaderProgram,
	name: string,
) -> (
	location: i32,
	found: bool,
) {
	uniform, found_ := uniforms[name]
	found = found_
	if !found_ {
		return
	}

	location = uniform.location
	return
}

set_uniform_i32 :: proc(using self: ^ShaderProgram, name: string, val: i32) {
	location, found := get_uniform_location(self, name)
	if found {
		gl.Uniform1i(location, val)
	}
}

set_uniform_f32 :: proc(using self: ^ShaderProgram, name: string, val: f32) {
	location, found := get_uniform_location(self, name)
	if found {
		gl.Uniform1f(i32(location), val)
	}
}

set_uniform_bool :: proc(using self: ^ShaderProgram, name: string, val: bool) {
	set_uniform_i32(self, name, val ? 1 : 0)
}

set_uniform_vec2f :: proc(using self: ^ShaderProgram, name: string, val: [2]f32) {
	location, found := get_uniform_location(self, name)
	if found {
		vec := val
		gl.Uniform2fv(i32(location), 1, &vec[0])
	}
}

set_uniform_vec3f :: proc(using self: ^ShaderProgram, name: string, val: [3]f32) {
	location, found := get_uniform_location(self, name)
	if found {
		vec := val
		gl.Uniform3fv(i32(location), 1, &vec[0])
	}
}

set_uniform_vec4f :: proc(using self: ^ShaderProgram, name: string, val: [4]f32) {
	location, found := get_uniform_location(self, name)
	if found {
		vec := val
		gl.Uniform4fv(i32(location), 1, &vec[0])
	}
}

set_uniform_mat4f :: proc(using self: ^ShaderProgram, name: string, val: matrix[4, 4]f32) {
	location, found := get_uniform_location(self, name)
	if found {
		mat := val
		gl.UniformMatrix4fv(i32(location), 1, false, &mat[0][0])
	}
}

set_uniform :: proc {
	set_uniform_i32,
	set_uniform_f32,
	set_uniform_bool,
	set_uniform_vec2f,
	set_uniform_vec3f,
	set_uniform_vec4f,
	set_uniform_mat4f,
}

