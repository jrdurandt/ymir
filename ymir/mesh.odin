package ymir

import "core:log"
import gl "vendor:OpenGL"

POSITION_INDEX :: 0
TEX_COORD_INDEX :: 1
NORMAL_INDEX :: 2
COLOR_INDEX :: 3
TANGENT_INDEX :: 4
BITANGENT_INDEX :: 5


VertexAttribDesc :: struct {
	index:     int,
	size:      int,
	type:      Type,
	normalize: bool,
	offset:    uintptr,
}

Mesh :: struct($Vertex: typeid) {
	vao, vbo, ebo: u32,
	vertices:      [dynamic]Vertex,
	indices:       [dynamic]u16,
	_index_count:  int,
}

mesh_create_raw :: proc(
	vertices: []$Vertex,
	indices: []u16,
	vertex_attribs: []VertexAttribDesc,
) -> (
	mesh: Mesh(Vertex),
) {
	mesh._index_count = len(indices)

	gl.CreateVertexArrays(1, &mesh.vao)
	gl.BindVertexArray(mesh.vao)

	gl.CreateBuffers(1, &mesh.vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, mesh.vbo)
	gl.BufferData(
		gl.ARRAY_BUFFER,
		len(vertices) * size_of(Vertex),
		raw_data(vertices),
		gl.STATIC_DRAW,
	)

	gl.CreateBuffers(1, &mesh.ebo)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.ebo)
	gl.BufferData(
		gl.ELEMENT_ARRAY_BUFFER,
		len(indices) * size_of(u16),
		raw_data(indices),
		gl.STATIC_DRAW,
	)

	for attrib in vertex_attribs {
		gl.EnableVertexAttribArray(u32(attrib.index))
		gl.VertexAttribPointer(
			u32(attrib.index),
			i32(attrib.size),
			u32(attrib.type),
			attrib.normalize,
			size_of(Vertex),
			attrib.offset,
		)
	}

	gl.BindVertexArray(0)
	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0)

	mesh.vertices = make([dynamic]Vertex)
	append(&mesh.vertices, ..vertices[:])

	mesh.indices = make([dynamic]u16)
	append(&mesh.indices, ..indices[:])

	return
}

mesh_create_soa :: proc(
	vertices: #soa[]$Vertex,
	indices: []u16,
	vertex_attribs: []VertexAttribDesc,
) -> Mesh(Vertex) {
	vertices := flatten_vertices(vertices)
	defer delete(vertices)
	return mesh_create_raw(vertices, indices, vertex_attribs)
}

mesh_create :: proc {
	mesh_create_raw,
	mesh_create_soa,
}

//Is there a built-in Odin method that can flatten an SOA to an AOS?
@(private)
flatten_vertices :: proc(vertices: #soa[]$Vertex) -> []Vertex {
	vertex_count := len(vertices)
	out := make([]Vertex, vertex_count)
	for i in 0 ..< vertex_count {
		vertex := vertices[i]
		out[i] = vertex
	}
	return out
}

mesh_delete :: proc(using mesh: ^Mesh($Vertex)) {
	delete(vertices)
	delete(indices)

	gl.DeleteVertexArrays(1, &vao)
	gl.DeleteBuffers(1, &vbo)
	gl.DeleteBuffers(1, &ebo)
}

mesh_draw :: proc(using self: ^Mesh($Vertex), count: i32 = 0, wireframe := false) {
	if wireframe {
		gl.Disable(gl.CULL_FACE)
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)
	}
	gl.BindVertexArray(vao)
	gl.DrawElements(gl.TRIANGLES, count > 0 ? count : i32(_index_count), gl.UNSIGNED_SHORT, nil)
	gl.BindVertexArray(0)
	if wireframe {
		gl.Enable(gl.CULL_FACE)
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
	}
}

mesh_update_vertices :: proc(using self: ^Mesh($Vertex)) {
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferSubData(gl.ARRAY_BUFFER, 0, len(vertices) * size_of(Vertex), raw_data(vertices))
	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
}

mesh_update_indices :: proc(using self: ^Mesh($Vertex)) {
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo)
	gl.BufferSubData(gl.ELEMENT_ARRAY_BUFFER, 0, len(indices) * size_of(u16), raw_data(indices))
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0)
}

mesh_map_vertex_buffer :: proc(
	using self: ^Mesh($Vertex),
	access: u32 = gl.WRITE_ONLY,
) -> []Vertex {
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	ptr := gl.MapBuffer(gl.ARRAY_BUFFER, access)

	return ([^]Vertex)(ptr)[:(len(vertices))]
}

mesh_unmap_vertex_buffer :: proc() {
	gl.UnmapBuffer(gl.ARRAY_BUFFER)
}

mesh_map_index_buffer :: proc(using self: ^Mesh($Vertex), access: u32 = gl.WRITE_ONLY) -> []u16 {
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo)
	ptr := gl.MapBuffer(gl.ELEMENT_ARRAY_BUFFER, access)

	return ([^]u16)(ptr)[:(len(indices))]
}

mesh_unmap_index_buffer :: proc() {
	gl.UnmapBuffer(gl.ELEMENT_ARRAY_BUFFER)
}

