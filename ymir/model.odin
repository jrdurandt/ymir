package ymir

import "core:log"
import "core:mem"
import "core:slice"
import "core:strings"

import gltf "vendor:cgltf"

import mikk "../libs/mikktspace-odin"

ModelVertex :: struct {
	position:  [3]f32,
	tex_coord: [2]f32,
	normal:    [3]f32,
	tangent:   [3]f32,
	bitangent: [3]f32,
}

MODEL_VERTEX_ATTRIBS :: []VertexAttribDesc {
	{
		index = 0,
		size = 3,
		type = .FLOAT,
		normalize = false,
		offset = offset_of(ModelVertex, position),
	},
	{
		index = 1,
		size = 2,
		type = .FLOAT,
		normalize = false,
		offset = offset_of(ModelVertex, tex_coord),
	},
	{
		index = 2,
		size = 3,
		type = .FLOAT,
		normalize = false,
		offset = offset_of(ModelVertex, normal),
	},
	{
		index = 3,
		size = 3,
		type = .FLOAT,
		normalize = false,
		offset = offset_of(ModelVertex, tangent),
	},
	{
		index = 4,
		size = 3,
		type = .FLOAT,
		normalize = false,
		offset = offset_of(ModelVertex, bitangent),
	},
}

MaterialType :: enum {
	ALBEDO,
	NORMAL,
}

Model :: struct {
	meshes:    [dynamic]Mesh(ModelVertex),
	materials: map[MaterialType]Texture,
}

model_load :: proc(path: string) -> (model: Model) {
	options := gltf.options{}
	c_path := strings.clone_to_cstring(path)
	defer delete(c_path)

	data, result := gltf.parse_file(options, c_path)
	log.assertf(result == .success, "Failed to parse model: {}", path)

	result = gltf.load_buffers(options, data, c_path)
	log.assertf(result == .success, "Failed to load model buffer data")

	mesh_count := len(data.meshes)
	model.meshes = make([dynamic]Mesh(ModelVertex), 0, mesh_count)
	for m in 0 ..< mesh_count {
		mesh := data.meshes[m]

		for p in 0 ..< len(mesh.primitives) {
			primitive := mesh.primitives[p]
			assert(primitive.type == .triangles, "Only triangles are supported")
			assert(primitive.indices != nil, "Indices are required")

			positions: [][3]f32
			defer delete(positions)

			tex_coords: [][2]f32
			defer delete(tex_coords)

			normals: [][3]f32
			defer delete(normals)

			for a in 0 ..< len(primitive.attributes) {
				attrib := primitive.attributes[a]
				vertex_accessor := attrib.data

				#partial switch (attrib.type) {
				case .position:
					positions = make([][3]f32, vertex_accessor.count)

					if vertex_accessor.type == .vec3 && vertex_accessor.component_type == .r_32f {
						load_attribute(vertex_accessor, f32, 3, &positions)
					}
				case .texcoord:
					tex_coords = make([][2]f32, vertex_accessor.count)

					if vertex_accessor.type == .vec2 && vertex_accessor.component_type == .r_32f {
						load_attribute(vertex_accessor, f32, 2, &tex_coords)
					}
				case .normal:
					normals = make([][3]f32, vertex_accessor.count)

					if vertex_accessor.type == .vec3 && vertex_accessor.component_type == .r_32f {
						load_attribute(vertex_accessor, f32, 3, &normals)
					}
				}
			}

			index_accessor := primitive.indices
			assert(
				index_accessor.component_type == .r_16u,
				"Only unsigned short indices supported",
			)
			indices_count := index_accessor.count
			indices := load_buffer_view_data(index_accessor, u16)[:indices_count]

			tangents := make([][3]f32, len(positions))
			defer delete(tangents)

			bitangents := make([][3]f32, len(positions))
			defer delete(bitangents)

			mikk.generate_tangent_space(
				positions,
				tex_coords,
				normals,
				indices,
				tangents,
				bitangents,
			)

			model_vertices: #soa[]ModelVertex = soa_zip(
				positions,
				tex_coords,
				normals,
				tangents,
				bitangents,
			)

			mesh := mesh_create(model_vertices, indices, MODEL_VERTEX_ATTRIBS)
			append(&model.meshes, mesh)
		}
	}

	material_count := len(data.materials)
	model.materials = make(map[MaterialType]Texture, material_count)
	for m in 0 ..< material_count {
		material := data.materials[m]

		if material.has_pbr_metallic_roughness {
			if material.pbr_metallic_roughness.base_color_texture.texture != nil {
				image := image_load(
					material.pbr_metallic_roughness.base_color_texture.texture.image_,
				)
				albedo_texture := texture_create_2d(&image)
				model.materials[.ALBEDO] = albedo_texture
			}
		} else if material.has_pbr_specular_glossiness {
			if material.pbr_specular_glossiness.diffuse_texture.texture != nil {
				image := image_load(
					material.pbr_specular_glossiness.diffuse_texture.texture.image_,
				)
				albedo_texture := texture_create_2d(&image)
				model.materials[.ALBEDO] = albedo_texture
			}
		}

		if material.normal_texture.texture != nil {
			image := image_load(material.normal_texture.texture.image_)
			normal_texture := texture_create_2d(&image)
			model.materials[.NORMAL] = normal_texture
		}
	}

	return
}

@(private)
load_buffer_view_data :: proc(accessor: ^gltf.accessor, $data_type: typeid) -> [^]data_type {
	buffer_view_offset := accessor.buffer_view.offset / size_of(data_type)
	buffer_offset := accessor.offset / size_of(data_type)
	buffer := cast([^]data_type)mem.ptr_offset(
		cast(^u8)accessor.buffer_view.buffer.data,
		accessor.buffer_view.offset,
	)
	return buffer
}

@(private)
load_attribute :: proc(
	accessor: ^gltf.accessor,
	$data_type: typeid,
	$num_comp: int,
	dst: ^[][num_comp]data_type,
) {
	buffer := load_buffer_view_data(accessor, data_type)
	n := 0
	for k in 0 ..< int(accessor.count) {
		v: [num_comp]data_type = {}
		for l in 0 ..< num_comp {
			v[l] = buffer[n + l]
		}
		dst[k] = v
		n += int(accessor.stride / size_of(data_type))
	}
}

model_delete :: proc(using self: ^Model) {
	for &mesh in meshes {
		mesh_delete(&mesh)
	}
	delete(meshes)

	for _, &material in materials {
		texture_delete(&material)
	}
	delete(materials)
}

model_draw :: proc(using self: ^Model, wireframe := false) {
	bind_texture(&materials[.ALBEDO], 0)
	bind_texture(&materials[.NORMAL], 1)
	for &mesh in meshes {
		mesh_draw(&mesh, wireframe = wireframe)
	}
}

