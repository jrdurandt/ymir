package ymir

import la "core:math/linalg"

Camera :: struct {
	position:   [3]f32,
	target:     [3]f32,
	up:         [3]f32,
	near, far:  f32,
	view:       matrix[4, 4]f32,
	projection: matrix[4, 4]f32,
}

PerspectiveCamera :: struct {
	fovy:         f32,
	aspect_ratio: f32,
	using base:   Camera,
}

camera_create_perspective :: proc(
	position, target, up: [3]f32,
	fovy, aspect_ratio: f32,
	near, far: f32,
) -> PerspectiveCamera {
	return PerspectiveCamera {
		position = position,
		target = target,
		up = up,
		near = near,
		far = far,
		fovy = fovy,
		aspect_ratio = aspect_ratio,
		view = la.matrix4_look_at(position, target, up),
		projection = la.matrix4_perspective(fovy, aspect_ratio, near, far),
	}
}

camera_update_view :: proc(using camera: ^Camera) {
	view = la.matrix4_look_at(position, target, up)
}

camera_update_projection :: proc(using camera: ^PerspectiveCamera) {
	projection = la.matrix4_perspective(fovy, aspect_ratio, near, far)
}

camera_forward :: proc(using camera: Camera) -> [3]f32 {
	return la.normalize(target - position)
}

camera_right :: proc(using camera: Camera) -> [3]f32 {
	forward := camera_forward(camera)
	return la.normalize(la.cross(forward, up))
}

camera_pitch :: proc(using camera: ^Camera, angle: f32) {
	target_position := target - position
	right := camera_right(camera^)
	rotation := la.matrix3_rotate(angle, right)
	target_position = rotation * target_position
	target = position + target_position
}

camera_yaw :: proc(using camera: ^Camera, angle: f32) {
	target_position := target - position
	rotation := la.matrix3_rotate(angle, up)
	target_position = rotation * target_position
	target = position + target_position
}

camera_rotate :: proc(using camera: ^Camera, rot: [3]f32) {
	camera_pitch(camera, rot.x)
	camera_yaw(camera, rot.y)
}

camera_move_forward :: proc(using camera: ^Camera, distance: f32) {
	forward := la.normalize(target - position) * distance
	position += forward
	target += forward
}

camera_move_up :: proc(using camera: ^Camera, distance: f32) {
	cam_up := up * distance
	position += cam_up
	target += cam_up
}

camera_move_right :: proc(using camera: ^Camera, distance: f32) {
	forward := la.normalize(target - position)
	right := la.normalize(la.cross(forward, up)) * distance
	position += right
	target += right
}

camera_move :: proc(using camera: ^Camera, direction: [3]f32) {
	camera_move_right(camera, direction.x)
	camera_move_up(camera, direction.y)
	camera_move_forward(camera, direction.z)
}

